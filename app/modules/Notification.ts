import {android as AndroidApp} from "application";

export class Notification{
	private _tone = android.media.RingtoneManager.TYPE_NOTIFICATION;
	private _icon = android.R.drawable.btn_star;
	private _appActivity = AndroidApp.foregroundActivity;
	private _appContext = AndroidApp.context;
	private _nBuilder;

	constructor(private _title:string,private _message:string){
		this._nBuilder = new android.support.v4.app.NotificationCompat.Builder(this._appActivity);
		this._resolveMetaData();
	}

	public lunch(){
		let notificationManager = this._appContext.getSystemService("notification");
		notificationManager.notify(0,this._nBuilder.build());
	}

	public addAction(info,action:string,callback){
		action = `com.ThreeThingsDone.${action.toUpperCase()}`;
		AndroidApp.registerBroadcastReceiver(action,callback);

		let intent = new android.content.Intent(action);
		let serviceIntent = android.app.PendingIntent.getBroadcast(this._appContext,5,intent,268435456);
		this._nBuilder.addAction(android.R.drawable.ic_lock_idle_alarm,info,serviceIntent);
	}

	private _resolveMetaData(){
		this._nBuilder.setContentTitle(this._title);
		this._nBuilder.setContentText(this._message);
		this._nBuilder.setSmallIcon(this._icon);

		let toneUri:string = android.media.RingtoneManager.getDefaultUri(this._tone).toString();
		this._nBuilder.setSound(android.net.Uri.parse(toneUri));
	};

}
