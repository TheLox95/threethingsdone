"use strict";
var application_1 = require("application");
var Notification = (function () {
    function Notification(_title, _message) {
        this._title = _title;
        this._message = _message;
        this._tone = android.media.RingtoneManager.TYPE_NOTIFICATION;
        this._icon = android.R.drawable.btn_star;
        this._appActivity = application_1.android.foregroundActivity;
        this._appContext = application_1.android.context;
        this._nBuilder = new android.support.v4.app.NotificationCompat.Builder(this._appActivity);
        this._resolveMetaData();
    }
    Notification.prototype.lunch = function () {
        var notificationManager = this._appContext.getSystemService("notification");
        notificationManager.notify(0, this._nBuilder.build());
    };
    Notification.prototype.addAction = function (info, action, callback) {
        action = "com.ThreeThingsDone." + action.toUpperCase();
        application_1.android.registerBroadcastReceiver(action, callback);
        var intent = new android.content.Intent(action);
        var serviceIntent = android.app.PendingIntent.getBroadcast(this._appContext, 5, intent, 268435456);
        this._nBuilder.addAction(android.R.drawable.ic_lock_idle_alarm, info, serviceIntent);
    };
    Notification.prototype._resolveMetaData = function () {
        this._nBuilder.setContentTitle(this._title);
        this._nBuilder.setContentText(this._message);
        this._nBuilder.setSmallIcon(this._icon);
        var toneUri = android.media.RingtoneManager.getDefaultUri(this._tone).toString();
        this._nBuilder.setSound(android.net.Uri.parse(toneUri));
    };
    ;
    return Notification;
}());
exports.Notification = Notification;
//# sourceMappingURL=Notification.js.map