"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var HardTask_1 = require("./models/HardTask");
var MediumTask_1 = require("./models/MediumTask");
var EasyTask_1 = require("./models/EasyTask");
var dialogs_1 = require("ui/dialogs");
var ApplicationSettings = require("application-settings");
var animation_1 = require("ui/animation");
var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.hardTask = new HardTask_1.default("", this.hardTaskView);
        this.mediumTask = new MediumTask_1.default("", this.mediumTaskView);
        this.easyTask = new EasyTask_1.default("", this.easyTaskView);
        this.taskList = [];
        this.taskList.push(this.hardTask);
        this.taskList.push(this.mediumTask);
        this.taskList.push(this.easyTask);
        this.asingEventToTasks(this.taskList, "tap", this.resolveStates, this);
        setInterval(function () { _this.tick(); }, 60000);
    };
    AppComponent.prototype.tick = function () {
        var _this = this;
        this.taskList.forEach(function (item) {
            if (item.state == "Working") {
                item.time = (item.time - 1);
                ApplicationSettings.setString(item.unasignedMessage + "time", item.time.toString());
            }
            if (item.time == 0) {
                item.state = "Finished";
                _this.updateAllTask();
            }
        });
    };
    AppComponent.prototype.resolveStates = function (e) {
        var taskClicked = this.getTaskAsignedToView(e.object);
        var remaningTasks = this.getAllTaskExcluding(taskClicked);
        if (this.netherTaskInWorking() && taskClicked.state == "Asigned") {
            taskClicked.state = "Working";
            if (remaningTasks[0].state != "Unasigned") {
                remaningTasks[0].state = "Halted";
            }
            if (remaningTasks[1].state != "Unasigned") {
                remaningTasks[1].state = "Halted";
            }
            // TODO: Fix this! Code duplicated! Look for animax!
            remaningTasks[0].view.animate({ scale: { x: 0.4, y: 0.4 } });
            remaningTasks[1].view.animate({ scale: { x: 0.4, y: 0.4 } });
            taskClicked.view.animate({
                scale: { x: 0.4, y: 0.4 }
            })
                .then(function () { return taskClicked.view.animate({ scale: { x: 1, y: 1 } }); })
                .then(function () { return taskClicked.showFullScreen(); });
            this.updateAllTask();
            return;
        }
        if (this.existTaskInWorkingState() && this.taskClickedHasWorkingState(taskClicked)) {
            this.taskList.forEach(function (item) {
                if (item.state != "Unasigned") {
                    item.state = "Asigned";
                }
            });
            taskClicked.view.animate({
                scale: { x: 1.2, y: 1.2 }
            }).then(function () {
                taskClicked.view.animate({ scale: { x: 0.4, y: 0.4 } });
            }).then(function () {
                taskClicked.view.parent._applyXmlAttribute("rows", "*,*,*");
            }).then(function () {
                taskClicked.view.animate({ scale: { x: 1, y: 1 } });
            });
            var animaxOptions = [
                { target: remaningTasks[0].view, scale: { x: 1, y: 1 } },
                { target: remaningTasks[1].view, scale: { x: 1, y: 1 } }
            ];
            var animax = new animation_1.Animation(animaxOptions);
            animax.play();
            this.updateAllTask();
            return;
        }
        if (taskClicked.state == "Unasigned") {
            dialogs_1.prompt("Write the name of the task:", "").then(function (r) {
                if (r.result == true) {
                    taskClicked.name = r.text;
                    taskClicked.state = "Asigned";
                    ApplicationSettings.setString(taskClicked.unasignedMessage, taskClicked.name);
                    taskClicked.update();
                }
            });
        }
    };
    AppComponent.prototype.asingEventToTasks = function (task, typeEvent, callbackFunc, scopeToUse) {
        task.forEach(function (item, index, taskList) {
            item.view.on(typeEvent, callbackFunc, scopeToUse);
        });
    };
    AppComponent.prototype.taskClickedHasWorkingState = function (task) {
        return (task.state == "Working");
    };
    AppComponent.prototype.existTaskInWorkingState = function () {
        var exist = false;
        this.taskList.forEach(function (item) {
            if (item.state == "Working") {
                exist = true;
            }
        });
        return exist;
    };
    AppComponent.prototype.getAllTaskExcluding = function (taskToExclude) {
        var arrTask = [];
        this.taskList.forEach(function (item) {
            if (item != taskToExclude) {
                arrTask.push(item);
            }
        });
        return arrTask;
    };
    AppComponent.prototype.netherTaskInWorking = function () {
        var amount = 0;
        this.taskList.forEach(function (item) {
            if (item.state != "Working") {
                amount++;
            }
        });
        if (amount == this.taskList.length) {
            return true;
        }
        return false;
    };
    AppComponent.prototype.getTaskAsignedToView = function (view) {
        if (view.id == this.hardTaskView.nativeElement.id) {
            return this.hardTask;
        }
        else if (view.id == this.mediumTaskView.nativeElement.id) {
            return this.mediumTask;
        }
        return this.easyTask;
    };
    AppComponent.prototype.updateAllTask = function () {
        this.taskList.forEach(function (item, index, taskList) {
            console.log("" + item.state);
            item.update();
        });
    };
    __decorate([
        core_1.ViewChild("taskHard"), 
        __metadata('design:type', core_1.ElementRef)
    ], AppComponent.prototype, "hardTaskView", void 0);
    __decorate([
        core_1.ViewChild("taskMedium"), 
        __metadata('design:type', core_1.ElementRef)
    ], AppComponent.prototype, "mediumTaskView", void 0);
    __decorate([
        core_1.ViewChild("taskEasy"), 
        __metadata('design:type', core_1.ElementRef)
    ], AppComponent.prototype, "easyTaskView", void 0);
    AppComponent = __decorate([
        core_1.Component({
            selector: "my-app",
            templateUrl: "app.component.html",
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map