import {Label} from "ui/label";
import {GridLayout} from "ui/layouts/grid-layout";
import {GestureTypes} from "ui/gestures";
import {Color} from "color";
import {linearGradient} from "../gradient";

export class AsignedManager{
	taskNameLabel:Label;
	timeLabel:Label;

	constructor(private taskLayout:GridLayout,private task){
		this.taskNameLabel = <Label>(<GridLayout>taskLayout.getChildAt(1)).getChildAt(0);
		this.timeLabel = <Label>(<GridLayout>taskLayout.getChildAt(1)).getChildAt(1);
		linearGradient(taskLayout,this.task.colors);
	}

	update(){
		//this.taskLayout.parent._applyXmlAttribute("rows","*,*,*");
		this.taskNameLabel.fontSize = 52;
		this.timeLabel.opacity = 1;

	}

}