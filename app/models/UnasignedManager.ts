import {Label} from "ui/label";
import {GridLayout} from "ui/layouts/grid-layout";
import {linearGradient} from "../gradient";
import {Task} from "./Task";


export class UnasignedManager{
	taskNameLabel:Label;
	timeLabel:Label;

	constructor(private taskLayout:GridLayout,private task:Task){
		linearGradient(this.taskLayout,["#9A9A9A","#696969"]);
		this.timeLabel = <Label>(<GridLayout>taskLayout.getChildAt(1)).getChildAt(1);
		this.taskNameLabel = <Label>(<GridLayout>taskLayout.getChildAt(1)).getChildAt(0);
	}

	update(){
		(<GridLayout>this.taskLayout.getChildAt(1))._applyXmlAttribute("rows","*");		
		this.timeLabel.opacity = 0;
	}

}