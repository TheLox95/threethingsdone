"use strict";
var HaltedManager_1 = require("./HaltedManager");
var WorkingManager_1 = require("./WorkingManager");
var FinishedManager_1 = require("./FinishedManager");
var AsignedManager_1 = require("./AsignedManager");
var UnasignedManager_1 = require("./UnasignedManager");
var ApplicationSettings = require("application-settings");
var Task = (function () {
    function Task(_name, _time, _view) {
        this._name = _name;
        this._time = _time;
        this._view = _view;
        this._state = "Unasigned";
        /* The unasignedMessage of the task is use as key to save the name of the task*/
        var date = new Date();
        var todayDate = date.getDay().toString() + "/" + date.getMonth().toString();
        if (ApplicationSettings.getString("lastDayStarted") == undefined) {
            ApplicationSettings.setString("lastDayStarted", todayDate);
        }
        if (ApplicationSettings.getString("lastDayStarted") != todayDate) {
            ApplicationSettings.clear();
        }
    }
    Task.prototype._initialize = function () {
        this._resolveName();
        this._resolveTime();
        this._resolveInnerManager();
        this.update();
    };
    Task.prototype._resolveName = function () {
        if (ApplicationSettings.getString(this._unasignedMessage) != undefined) {
            this._state = "Asigned";
            this._name = ApplicationSettings.getString(this._unasignedMessage);
        }
        else {
            this._state = "Unasigned";
            this._name = this._unasignedMessage;
        }
        this.update();
    };
    Task.prototype._resolveTime = function () {
        if (ApplicationSettings.getString(this._unasignedMessage + "time") == undefined) {
            ApplicationSettings.setString(this._unasignedMessage + "time", this._time.toString());
        }
        else {
            this._time = parseInt(ApplicationSettings.getString(this._unasignedMessage + "time"));
            if (this._time == 0) {
                this._state = "Finished";
            }
        }
    };
    Task.prototype._resolveInnerManager = function () {
        if (this._state == "Unasigned") {
            this._taskInnerManager = new UnasignedManager_1.UnasignedManager(this._view.nativeElement, this);
        }
        if (this._state == "Asigned") {
            this._taskInnerManager = new AsignedManager_1.AsignedManager(this._view.nativeElement, this);
        }
        if (this._state == "Working") {
            this._taskInnerManager = new WorkingManager_1.WorkingManager(this._view.nativeElement, this);
        }
        if (this._state == "Halted") {
            this._taskInnerManager = new HaltedManager_1.HaltedManager(this._view.nativeElement);
        }
        if (this._state == "Finished") {
            this._taskInnerManager = new FinishedManager_1.FinishedManager(this._view.nativeElement);
        }
    };
    /**
     * Updates the state of the task
     * @return {void}
     */
    Task.prototype.update = function () {
        this._resolveInnerManager();
        this._taskInnerManager.update();
    };
    Object.defineProperty(Task.prototype, "view", {
        /**
         * Returns the grid xml element corespondent to the task
         * @return {GridLayout} XML Element
         */
        get: function () {
            return this._view.nativeElement;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Task.prototype, "state", {
        get: function () {
            return this._state;
        },
        set: function (state) {
            this._state = state;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Task.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (name) {
            this._name = name;
            this._view.nativeElement.getChildAt(1).getChildAt(0).text = name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Task.prototype, "time", {
        get: function () {
            return this._time;
        },
        set: function (time) {
            this._time = time;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Task.prototype, "colors", {
        get: function () {
            return this._colors;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Task.prototype, "unasignedMessage", {
        get: function () {
            return this._unasignedMessage;
        },
        enumerable: true,
        configurable: true
    });
    return Task;
}());
exports.Task = Task;
//# sourceMappingURL=Task.js.map