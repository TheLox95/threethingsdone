import {Task} from "./Task";
import {ElementRef} from "@angular/core";
import {GridLayout} from "ui/layouts/grid-layout";
import {linearGradient} from "../gradient";

export default class MediumTask extends Task{
	_colors = ["#002AFF","#80AAFF"];
	_unasignedMessage = "Write the second most challenge task of your day...";

	constructor(name:string,mediumTask:ElementRef){
		super(name,30,mediumTask);
		this._initialize();
	}

	showFullScreen(){
		(<GridLayout>this._view.nativeElement).parent._applyXmlAttribute("rows","0,*,0");
	}
}
