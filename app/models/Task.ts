import {ElementRef} from "@angular/core";
import {linearGradient} from "../gradient";
import {GridLayout} from "ui/layouts/grid-layout";
import {Color} from "color";
import {Label} from "ui/label";

import {HaltedManager} from "./HaltedManager";
import {WorkingManager} from "./WorkingManager";
import {FinishedManager} from "./FinishedManager";
import {AsignedManager} from "./AsignedManager";
import {UnasignedManager} from "./UnasignedManager";
import * as ApplicationSettings from "application-settings";

export type taskStates = "Asigned" | "Unasigned" | "Working" | "Halted" | "Finished";

export abstract class Task{
	protected _state: taskStates= "Unasigned";
	protected _taskInnerManager;
	protected _colors:string[];
	protected _unasignedMessage:string;

	abstract showFullScreen():void;


	constructor(protected _name:string, protected _time:number,protected _view:ElementRef){
		/* The unasignedMessage of the task is use as key to save the name of the task*/
		let date = new Date();
		let todayDate:string = `${date.getDay().toString()}/${date.getMonth().toString()}`;

		if(ApplicationSettings.getString("lastDayStarted") == undefined) {
			ApplicationSettings.setString("lastDayStarted",todayDate);
		}

		if(ApplicationSettings.getString("lastDayStarted") != todayDate) {
			ApplicationSettings.clear();
		}
		
	}

	protected _initialize(){
		this._resolveName();
		this._resolveTime();
		this._resolveInnerManager();
		this.update();
	}

	protected _resolveName(){
		if(ApplicationSettings.getString(this._unasignedMessage) != undefined) {
			this._state= "Asigned";
			this._name = ApplicationSettings.getString(this._unasignedMessage);
		}else{
			this._state= "Unasigned";
			this._name = this._unasignedMessage;			
		}

		this.update();
	}

	protected _resolveTime(){
		if(ApplicationSettings.getString(`${this._unasignedMessage}time`) == undefined) {
			ApplicationSettings.setString(`${this._unasignedMessage}time`,this._time.toString());
		}else{
			this._time = parseInt(ApplicationSettings.getString(`${this._unasignedMessage}time`));
			if(this._time == 0) {
				this._state = "Finished";
			}
		}
	}

	private _resolveInnerManager(){
		if(this._state == "Unasigned") {
			this._taskInnerManager = new UnasignedManager(this._view.nativeElement,this);
		}

		if(this._state == "Asigned"){
			this._taskInnerManager = new AsignedManager(this._view.nativeElement,this);
		}

		if(this._state == "Working"){
			this._taskInnerManager = new WorkingManager(this._view.nativeElement,this);
		}

		if(this._state == "Halted"){
			this._taskInnerManager = new HaltedManager(this._view.nativeElement);
		}

		if(this._state == "Finished"){
			this._taskInnerManager = new FinishedManager(this._view.nativeElement);
		}
	}

	/**
     * Updates the state of the task
     * @return {void}
     */
	update(){
		this._resolveInnerManager();
		this._taskInnerManager.update();
	}

	/**
	 * Returns the grid xml element corespondent to the task
	 * @return {GridLayout} XML Element
	 */
	get view():GridLayout{
		return this._view.nativeElement
	}

	set state(state:taskStates){
		this._state = state;
	}

	get state():taskStates{
		return this._state
	}

	get name():string{
		return this._name;
	}

	set name(name:string){
		this._name = name;
		(<Label>(<GridLayout>(<GridLayout>this._view.nativeElement).getChildAt(1)).getChildAt(0)).text = name;
	}

	
	public get time() {
		return this._time;
	}

	public set time(time){
		this._time = time;
	}

	public get colors(){
		return this._colors;
	}

	public get unasignedMessage():string{
		return this._unasignedMessage;
	}
}