"use strict";
var HaltedManager = (function () {
    function HaltedManager(taskLayout) {
        this.taskLayout = taskLayout;
        this.taskNameLabel = taskLayout.getChildAt(1).getChildAt(0);
        this.timeLabel = taskLayout.getChildAt(1).getChildAt(1);
    }
    HaltedManager.prototype.update = function () {
        /* this only show the name of the tasks and hide the time*/
        this.taskLayout.getChildAt(1)._applyXmlAttribute("rows", "*");
        this.taskNameLabel.fontSize = 20;
        this.timeLabel.opacity = 0;
    };
    return HaltedManager;
}());
exports.HaltedManager = HaltedManager;
//# sourceMappingURL=HaltedManager.js.map