"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Task_1 = require("./Task");
var EasyTask = (function (_super) {
    __extends(EasyTask, _super);
    function EasyTask(name, easyTask) {
        _super.call(this, name, 15, easyTask);
        this._colors = ["#FCFF00", "#FEFFC7"];
        this._unasignedMessage = "Write the third most challenge task of your day...";
        this._initialize();
    }
    EasyTask.prototype.showFullScreen = function () {
        this._view.nativeElement.parent._applyXmlAttribute("rows", "0,0,*");
    };
    return EasyTask;
}(Task_1.Task));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = EasyTask;
//# sourceMappingURL=EasyTask.js.map