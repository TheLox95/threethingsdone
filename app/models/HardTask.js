"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Task_1 = require("./Task");
var HardTask = (function (_super) {
    __extends(HardTask, _super);
    function HardTask(name, hardView) {
        _super.call(this, name, 60, hardView);
        this._unasignedMessage = "Write the most challenge task of your day...";
        this._colors = ["#FF0000", "#F74D4D"];
        this._initialize();
    }
    HardTask.prototype.showFullScreen = function () {
        this._view.nativeElement.parent._applyXmlAttribute("rows", "*,0,0");
    };
    return HardTask;
}(Task_1.Task));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = HardTask;
//# sourceMappingURL=HardTask.js.map