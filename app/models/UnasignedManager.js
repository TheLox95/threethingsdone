"use strict";
var gradient_1 = require("../gradient");
var UnasignedManager = (function () {
    function UnasignedManager(taskLayout, task) {
        this.taskLayout = taskLayout;
        this.task = task;
        gradient_1.linearGradient(this.taskLayout, ["#9A9A9A", "#696969"]);
        this.timeLabel = taskLayout.getChildAt(1).getChildAt(1);
        this.taskNameLabel = taskLayout.getChildAt(1).getChildAt(0);
    }
    UnasignedManager.prototype.update = function () {
        this.taskLayout.getChildAt(1)._applyXmlAttribute("rows", "*");
        this.timeLabel.opacity = 0;
    };
    return UnasignedManager;
}());
exports.UnasignedManager = UnasignedManager;
//# sourceMappingURL=UnasignedManager.js.map