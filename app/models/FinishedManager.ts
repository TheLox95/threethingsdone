import {Label} from "ui/label";
import {GridLayout} from "ui/layouts/grid-layout";
import {linearGradient} from "../gradient";
import {schedule,ScheduleOptions,hasPermission} from "nativescript-local-notifications";
import {Notification} from "../modules/Notification";
import {android as AndroidApp} from "application";


export class FinishedManager{
	taskNameLabel:Label;
	timeLabel:Label;

	constructor(private taskLayout:GridLayout){
		this.timeLabel = <Label>(<GridLayout>taskLayout.getChildAt(1)).getChildAt(1);
	}

	showToast(){
		let toast = new android.widget.Toast.makeText(AndroidApp.context,"Extendiendo...", android.widget.Toast.LENGTH_SHORT);
		toast.show();

	}

	update(){
		linearGradient(this.taskLayout,["#ECCC4E","#F1D986"]);

		let notification = new Notification("ThreeThingsDone","Finalizada!");
		notification.addAction("Terminaste esta tarea!","close",this.showToast);
		notification.lunch();
	}

}
