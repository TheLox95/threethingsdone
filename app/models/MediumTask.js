"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Task_1 = require("./Task");
var MediumTask = (function (_super) {
    __extends(MediumTask, _super);
    function MediumTask(name, mediumTask) {
        _super.call(this, name, 30, mediumTask);
        this._colors = ["#002AFF", "#80AAFF"];
        this._unasignedMessage = "Write the second most challenge task of your day...";
        this._initialize();
    }
    MediumTask.prototype.showFullScreen = function () {
        this._view.nativeElement.parent._applyXmlAttribute("rows", "0,*,0");
    };
    return MediumTask;
}(Task_1.Task));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = MediumTask;
//# sourceMappingURL=MediumTask.js.map