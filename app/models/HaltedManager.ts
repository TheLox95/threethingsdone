import {Label} from "ui/label";
import {GridLayout} from "ui/layouts/grid-layout";

export class HaltedManager{
	taskNameLabel:Label;
	timeLabel:Label;

	constructor(private taskLayout:GridLayout){
		this.taskNameLabel = <Label>(<GridLayout>taskLayout.getChildAt(1)).getChildAt(0);
		this.timeLabel = <Label>(<GridLayout>taskLayout.getChildAt(1)).getChildAt(1);
	}

	update(){
		/* this only show the name of the tasks and hide the time*/
		(<GridLayout>this.taskLayout.getChildAt(1))._applyXmlAttribute("rows","*");		
		this.taskNameLabel.fontSize = 20;
		this.timeLabel.opacity = 0;

	}

}