"use strict";
var gradient_1 = require("../gradient");
var AsignedManager = (function () {
    function AsignedManager(taskLayout, task) {
        this.taskLayout = taskLayout;
        this.task = task;
        this.taskNameLabel = taskLayout.getChildAt(1).getChildAt(0);
        this.timeLabel = taskLayout.getChildAt(1).getChildAt(1);
        gradient_1.linearGradient(taskLayout, this.task.colors);
    }
    AsignedManager.prototype.update = function () {
        //this.taskLayout.parent._applyXmlAttribute("rows","*,*,*");
        this.taskNameLabel.fontSize = 52;
        this.timeLabel.opacity = 1;
    };
    return AsignedManager;
}());
exports.AsignedManager = AsignedManager;
//# sourceMappingURL=AsignedManager.js.map