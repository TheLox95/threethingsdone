import {Task} from "./Task";
import {ElementRef} from "@angular/core";
import {GridLayout} from "ui/layouts/grid-layout";
import {linearGradient} from "../gradient";

export default class EasyTask extends Task{
	_colors = ["#FCFF00","#FEFFC7"];
	_unasignedMessage = "Write the third most challenge task of your day...";

	constructor(name:string,easyTask:ElementRef){
		super(name,15,easyTask);
		this._initialize();
	}

	showFullScreen(){
		(<GridLayout>this._view.nativeElement).parent._applyXmlAttribute("rows","0,0,*");
	}

}
