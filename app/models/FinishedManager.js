"use strict";
var gradient_1 = require("../gradient");
var Notification_1 = require("../modules/Notification");
var application_1 = require("application");
var FinishedManager = (function () {
    function FinishedManager(taskLayout) {
        this.taskLayout = taskLayout;
        this.timeLabel = taskLayout.getChildAt(1).getChildAt(1);
    }
    FinishedManager.prototype.showToast = function () {
        var toast = new android.widget.Toast.makeText(application_1.android.context, "Extendiendo...", android.widget.Toast.LENGTH_SHORT);
        toast.show();
    };
    FinishedManager.prototype.update = function () {
        gradient_1.linearGradient(this.taskLayout, ["#ECCC4E", "#F1D986"]);
        var notification = new Notification_1.Notification("ThreeThingsDone", "Finalizada!");
        notification.addAction("Terminaste esta tarea!", "close", this.showToast);
        notification.lunch();
    };
    return FinishedManager;
}());
exports.FinishedManager = FinishedManager;
//# sourceMappingURL=FinishedManager.js.map