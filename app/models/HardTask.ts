import {Task} from "./Task";
import {ElementRef} from "@angular/core";
import {GridLayout} from "ui/layouts/grid-layout";
import {linearGradient} from "../gradient";

export default class HardTask extends Task{
	_unasignedMessage = "Write the most challenge task of your day...";
	_colors = ["#FF0000","#F74D4D"];

	constructor(name:string,hardView:ElementRef){
		super(name,60,hardView);
		this._initialize();
	}

	showFullScreen(){
		(<GridLayout>this._view.nativeElement).parent._applyXmlAttribute("rows","*,0,0");
	}
}
