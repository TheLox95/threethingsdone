"use strict";
var platform = require("platform");
var colorModule = require("color");
var Color = colorModule.Color;
var Color = colorModule.Color;
function linearGradient(viewId, colors) {
    console.log(platform.device.os);
    var _colors = [], _view = viewId, nativeView;
    if (_view) {
        nativeView = _view._nativeView;
    }
    else {
        throw TraceableException("Cannot find view '" + view + "' in page!");
    }
    if (!nativeView) {
        return;
    }
    colors.forEach(function (c, idx) {
        if (!(c instanceof Color)) {
            colors[idx] = new Color(c);
        }
    });
    if (platform.device.os === platform.platformNames.android) {
        console.log("android");
        var backgroundDrawable = nativeView.getBackground(), orientation = android.graphics.drawable.GradientDrawable.Orientation.RIGHT_LEFT, LINEAR_GRADIENT = 0;
        colors.forEach(function (c) {
            _colors.push(c.android);
        });
        if (!(backgroundDrawable instanceof android.graphics.drawable.GradientDrawable)) {
            backgroundDrawable = new android.graphics.drawable.GradientDrawable();
            backgroundDrawable.setColors(_colors);
            backgroundDrawable.setOrientation(orientation);
            backgroundDrawable.setGradientType(LINEAR_GRADIENT);
            var shape = new android.graphics.drawable.ShapeDrawable();
            console.log(_view.getActualSize().height);
            shape.getPaint().setShader(new android.graphics.LinearGradient(0, 0, 0, 500, _colors[0], _colors[1], android.graphics.Shader.TileMode.REPEAT));
            nativeView.setBackgroundDrawable(shape);
        }
    }
    else if (platform.device.os === platform.platformNames.ios) {
        console.log("ios");
        var view = root.ios.view;
        var colorsArray = NSMutableArray.alloc().initWithCapacity(2);
        colors.forEach(function (c) {
            colorsArray.addObject(interop.types.id(c.ios.CGColor));
        });
        var gradientLayer = CAGradientLayer.layer();
        gradientLayer.colors = colorsArray;
        gradientLayer.frame = view.bounds;
        view.layer.insertSublayerAtIndex(gradientLayer, 0);
    }
}
exports.linearGradient = linearGradient;
//# sourceMappingURL=gradient.js.map