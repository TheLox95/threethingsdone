import {Component, ViewChild, ElementRef, OnInit} from "@angular/core";
import {EventData} from "data/observable";

import {linearGradient} from "./gradient";
import {Task, taskStates} from "./models/Task";
import {GridLayout} from "ui/layouts/grid-layout";
import HardTask from "./models/HardTask";
import MediumTask from "./models/MediumTask";
import EasyTask from "./models/EasyTask";
import {prompt} from "ui/dialogs";
import * as ApplicationSettings from "application-settings";
import {Animation, AnimationDefinition} from "ui/animation";



@Component({
    selector: "my-app",
    templateUrl: "app.component.html",
})
export class AppComponent implements OnInit{
    hardTask: HardTask;
    mediumTask: MediumTask;
    easyTask: EasyTask;

    taskList: Task[];

    @ViewChild("taskHard") hardTaskView: ElementRef;
    @ViewChild("taskMedium") mediumTaskView: ElementRef;
    @ViewChild("taskEasy") easyTaskView: ElementRef

    ngOnInit(){
        
        this.hardTask = new HardTask("",this.hardTaskView);
        this.mediumTask = new MediumTask("",this.mediumTaskView);
        this.easyTask = new EasyTask("",this.easyTaskView);
        
        this.taskList = [];
        this.taskList.push(this.hardTask);
        this.taskList.push(this.mediumTask);
        this.taskList.push(this.easyTask);
        

        this.asingEventToTasks(this.taskList,"tap",this.resolveStates,this);
        setInterval(()=>{this.tick()},60000);
    }

    tick(){
       this.taskList.forEach((item:Task)=>{
            if(item.state == "Working") {
                item.time = (item.time - 1);
                ApplicationSettings.setString(`${item.unasignedMessage}time`,item.time.toString());
            }

            if(item.time == 0) {
                item.state = "Finished";
                this.updateAllTask();
            }
        });

    }

    resolveStates(e:EventData){
        let taskClicked = this.getTaskAsignedToView((<GridLayout>e.object));
        let remaningTasks = this.getAllTaskExcluding(taskClicked);

        if(this.netherTaskInWorking() && taskClicked.state == "Asigned") {
            taskClicked.state = "Working";
            if(remaningTasks[0].state != "Unasigned") {
                remaningTasks[0].state = "Halted";
            }
            if(remaningTasks[1].state != "Unasigned") {
                remaningTasks[1].state = "Halted";
            }

            // TODO: Fix this! Code duplicated! Look for animax!
            remaningTasks[0].view.animate({scale:{x:0.4,y:0.4}});
            remaningTasks[1].view.animate({scale:{x:0.4,y:0.4}});

            taskClicked.view.animate({
                scale:{x:0.4,y:0.4}
            })
            .then(() => taskClicked.view.animate({scale:{x:1,y:1}}))
            .then(() => taskClicked.showFullScreen());

            this.updateAllTask();
            return;
        }

        if(this.existTaskInWorkingState() && this.taskClickedHasWorkingState(taskClicked)){
            this.taskList.forEach((item:Task)=>{
                if(item.state != "Unasigned") {
                    item.state = "Asigned";
                }
            });

            taskClicked.view.animate({
                scale:{x:1.2,y:1.2}
            }).then(()=>{
                taskClicked.view.animate({scale:{x:0.4,y:0.4}})
            }).then(()=>{
                taskClicked.view.parent._applyXmlAttribute("rows","*,*,*");
            }).then(()=>{
                taskClicked.view.animate({scale:{x:1,y:1}})
            });

            let animaxOptions:Array<AnimationDefinition> = [
                {target: remaningTasks[0].view, scale:{x:1,y:1}},
                {target: remaningTasks[1].view, scale:{x:1,y:1}}
            ];

            let animax = new Animation(animaxOptions);

            animax.play();

            this.updateAllTask();
            return;
        }

        if(taskClicked.state == "Unasigned") {
            prompt("Write the name of the task:","").then((r)=>{
                if(r.result == true) {
                    taskClicked.name = r.text;
                    taskClicked.state = "Asigned";
                    ApplicationSettings.setString(taskClicked.unasignedMessage,taskClicked.name);
                    taskClicked.update();
                }
            });

            
        }
    }

    asingEventToTasks(task:Task[],typeEvent:string,callbackFunc,scopeToUse?){
        task.forEach((item:Task, index:number,taskList:Task[])=>{
            item.view.on(typeEvent,callbackFunc,scopeToUse);
        });
    }

    taskClickedHasWorkingState(task:Task):boolean{
        return (task.state == "Working");
    }

    existTaskInWorkingState():boolean{
        let exist:boolean = false;
        this.taskList.forEach((item:Task)=>{
            if(item.state == "Working") {
                exist = true;
            }
        });

        return exist;
    }

    getAllTaskExcluding(taskToExclude:Task):Task[]{
        let arrTask:Task[] = [];
        this.taskList.forEach((item:Task)=>{
            if(item != taskToExclude) {
                arrTask.push(item);
            }
        });

        return arrTask;
    }

    netherTaskInWorking():boolean{
        let amount:number = 0;
        this.taskList.forEach((item:Task)=>{
            if(item.state != "Working") {
                amount++;
            }
        });

        if(amount == this.taskList.length) {
            return true;
        }

        return false;
    }

    getTaskAsignedToView(view:GridLayout):Task{
       if(view.id == this.hardTaskView.nativeElement.id) {
           return this.hardTask;
       }else if(view.id == this.mediumTaskView.nativeElement.id){
           return this.mediumTask;
       }

       return this.easyTask;
    }

    updateAllTask(){
        this.taskList.forEach((item:Task, index:number,taskList:Task[])=>{
            console.log(`${item.state}`);
            item.update();
        });
    }
}
